
# Instalaciones necesarias

## ¿Que es Hardhat?

Es un entorno de desarrollo que permite compilar, probar y desplegar smart contracts. (tiene problemas con Node 15... utilizar 16+)

### `npm install --save-dev hardhat`

### `npm install --save-dev @nomiclabs/hardhat-waffle ethereum-waffle chai @nomiclabs/hardhat-ethers ethers`

## Open Zeppelin

### `npm install @openzeppelin/contracts`

`npx hardhat run .\scripts\sample-script.js`

# Compilación

`npx hardhat compile`

# Tests

`npx hardhat test`

# Deploy

`npx hardhat run .\scripts\sample-script.js --network rinkeby`

# Get Flat contracts

`npx hardhat flatten > Flattened.sol`

# History

https://rinkeby.etherscan.io/

- Greeter deployed to: 0xB189BaEa7585D96C302012149Ea2361737382F3a
- Greeter deployed to: 0x5Be595177218a88e65C1d3439530d23C2c20dDA3

Deploying contracts with the account: 0x7D6170Ded5EE2E03989c89D81B4A06C240a97dA5
Platzi Punks address: 0x9768DBcA5eE5b7Da23244FEfF09C5b3f95F845C9
(version final) Platzi Punks address: 0xdF244A0e56B7DEED158D8f9c5cDa3E4639995B98


https://ropsten.etherscan.io/

- Greeter deployed to: 0xB189BaEa7585D96C302012149Ea2361737382F3a